# package pswd

```go
package main

import (
	"fmt"

	"bitbucket.org/sergsoloviev/pswd"
)

func main() {

	pswd.Cost = 11

	hash, err := pswd.Set("ok_password")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(hash)

	ok, err := pswd.Check("ok_password", hash)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(ok)

	ok, err = pswd.Check("wrong_password", hash)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(ok)

	pswd.SecretKey = []byte("1234567891123456")

	hash_string, err := pswd.Encrypt([]byte("password"))
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(hash_string)

	var pswd string
	pswd, err = pswd.Decrypt(hash_string)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(pswd)

}
```
```bash
bash-3.2$ go run main.go
$2a$11$wgT7WoCi0Li/J9RijfsjB.WzdiniMMf507hwYakJ3xkTplhzx/21q
true
crypto/bcrypt: hashedPassword is not the hash of the given password
false
89/y8iu8wYjuwhcScljtrn2kQPZfMos0IQ1CRA==
password
```
